package view_client 

import (

	"fmt"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"errors"
	"net/http"
	"bitbucket.org/cornjacket/iot/message"
)

func (s *ViewHndlrService) SendPacket(packet message.UpPacket) (error) {

	var err error
	if s.Enabled {
		//fmt.Printf("ViewHndlrService: Sending packet to %s\n", s.URL("/packet"))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)
		err = Post(s.URL("/packet"), b)
	} else {
		fmt.Printf("ViewHndlrService is disabled. Unable to send packet.\n")
	}
	return err 

}


// The following data structures are taken from the ViewHandler's Packet Controller
// web backend will request packet to be retrieved and returned by this controller
type PacketReq struct {
        TsStart uint64  `json:"start"`
        TsEnd   uint64  `json:"end"`
        Eui     string  `json:"eui"`
}

func (d *PacketReq) Validate() error {
        if d.Eui == "" {
                return errors.New("Required Eui")
	}
	return nil
}

// UpPacket is a subset of message.UpPacket
type UpPacket struct {
        Dr   string  `json:"dr"`
        Ts   uint64  `json:"ts"`
        Ack  bool    `json:"ack"`
        Snr  float32 `json:"snr"`
        Data string  `json:"data"`
        Fcnt int     `json:"fcnt"`
        Freq uint64  `json:"freq"`
        Port int     `json:"port"`
        Rssi int     `json:"rssi"`
}

type PacketsResp struct {
        TsStart uint64  `json:"start"`
        TsEnd   uint64  `json:"end"`
        Eui     string  `json:"eui"`
        Packets []UpPacket      `json:"packets"`
}

func (s *ViewHndlrService) GetPackets(req PacketReq) (PacketsResp, error) {

	fmt.Printf("ViewHndlrService.GetPackets Eui: %s, start: %d, end: %d\n", req.Eui, req.TsStart, req.TsEnd)
	var resp PacketsResp
	if !s.Enabled {
		return resp, errors.New("client not enabled")
	}
	reqBody, err := json.Marshal(req)
	if err != nil {
               	return resp, err 
	}
	url := s.URL("/packet") // todo(DRT) - I think the view_handler is expecting the eui to be in the path...
	client := &http.Client{}
	httpReq, err := http.NewRequest("GET", url, bytes.NewBuffer(reqBody))
	if err != nil {
               	return resp, err 
	}
	httpReq.Header.Set("Content-type","application/json")
	if err != nil {
               	return resp, err 
	}
        response, err := client.Do(httpReq)
	if err != nil {
               	return resp, err 
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
               		return resp, err 
		}
		err = json.Unmarshal(contents, &resp)
		if err != nil {
               		return resp, err 
		}
	}
	return resp, err
}

