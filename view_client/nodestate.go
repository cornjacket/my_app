package view_client

import (
	"bytes"
	"encoding/json"
	"io/ioutil"

	"fmt"
	"net/http"
	"time"

)

// Should I make this into NodeState and use both for Send and Get funcitons? DRT This would mean
// I would need to cast from nodestate row into this nodestate type below.
// The following definition is taken from the View Handler's shadow copy of the view_nodestate table's row
type NodeState struct {
	Eui                    string    `json:"eui"`
	ValidOnboard           int       `json:"validonboard"`
	CreatedAt              time.Time `json:"createdat"`
	UpdatedAt              time.Time `json:"updatedat"`
	AppNum                 int       `json:"appnum"`
	AppApi                 int       `json:"appapi"`
	SysApi                 int       `json:"sysapi"`
	AppFwVersion           int       `json:"appfwversion"`
	SysFwVersion           int       `json:"sysfwversion"`
	ComApi                 int       `json:"comapi"`
	ComFwVersion           int       `json:"comfwversion"`
	LastPktRcvd            string    `json:"lastpktrcvd"`
	LastPktRcvdTs          int64     `json:"lastpktrcvdts"`
	LastVoltageRcvd        int       `json:"lastvoltagercvd"`
	LastVoltageRcvdTs      int64     `json:"lastvoltagercvdts"`
	LastResetCause         int       `json:"lastresetcause"`
	LastResetCauseTs       int64     `json:"lastresetcausets"`
	LastRssi               int       `json:"lastrssi"`
	LastRssiTs             int64     `json:"lastrssits"`
	LastSnr                int       `json:"lastsnr"`
	LastSnrTs              int64     `json:"lastsnrts"`
	NumDownstreams         int       `json:"numdownstreams"`
	NumClearQueries        int       `json:"numclearqueries"`
	AppLowPowerState       int       `json:"applowpowerstate"`
	GlobalDownstreamEnable int       `json:"globaldownstreamenable"`
	SystemDownstreamEnable int       `json:"systemdownstreamenable"`
	AppDownstreamEnable    int       `json:"appdownstreamenable"`
	TSlot                  int       `json:"tslot"`
	TSlotValid             int       `json:"tslotvalid"`
	TSlotAssignedTs        int64     `json:"tslotassignedts"`
	ActualStateA           int       `json:"actualstatea"`
	ActualStateB           int       `json:"actualstateb"`
	ExpectedStateA         int       `json:"expectedstatea"`
	ExpectedStateB         int       `json:"expectedstateb"`
}

func (s *ViewHndlrService) SendNodestate(nodestate *NodeState) error {

	var err error
	if s.Enabled {
		//fmt.Printf("ViewHndlrService: Sending nodestate to %s\n", s.URL("/nodestate"))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(*nodestate)
		err = Post(s.URL("/nodestate"), b)
	} else {
		fmt.Printf("ViewHndlrService is disabled. Unable to send nodestate.\n")
	}
	return err

}

func (s *ViewHndlrService) GetNodeState(eui string) (NodeState, error) {

	fmt.Printf("ViewHndlrService.GetNodeState Eui: %s\n", eui)
	var resp NodeState
	if !s.Enabled {
		return resp, nil // This should be a custom error indicating the service was not initialized
	}

	// TODO(drt) Check if valid, ie. eui is not empty. Check if user is authorized to get eui
	//if err != nil {
	//       	return resp, err
	//}

	url := s.URL("/nodestate/") + eui
	client := &http.Client{}
	httpReq, err := http.NewRequest("GET", url, nil)
	httpReq.Header.Set("Content-type", "application/json")
	if err != nil {
		return resp, err
	}
	response, err := client.Do(httpReq)

	if err != nil {
		return resp, err
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return resp, err
		}
		err = json.Unmarshal(contents, &resp)
		if err != nil {
			return resp, err
		}
	}
	return resp, err
}
