package view_client 

import (

	"bytes"
	"io/ioutil"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type ViewHndlrService struct {
	Hostname  string
	Port      string
	Enabled   bool
}

// Comment for testing modules

// TODO: There is code duplication with the Data struct. It lives within the app as well as this client lib. It should only live in one place,
//       probably in the client lib only.
// Note that Data type is copied from view_hndlr_4_app3_sys6/dataproc. By copying it instead of importing it as a dependency the event_hndlr
// can be built independent of the view_hndlr. It is imperative that any changes to Data in the view_hndlr uService be refected in this service,
// and vice versa.
type Data struct {
	Eui	string // this should be intrinsic_id, we can have LoraEui and Eui (intrinsic)
	Ts	uint64
	ValueA	int
	ValueB	int
        CorrId  uint64  `json:"corrid"`
        ClassId string  `json:"classid"`
        Hops 	int	`json:"hops"`
}

// TODO: Add error return value
// TODO: Validate Hostname and Port within reason
func (s *ViewHndlrService) Open(Hostname, Port string) error {
	s.Hostname = Hostname
	s.Port = Port
	s.Enabled = true
	return s.Ping()	
}

func (s *ViewHndlrService) URL(path string) string {
	if s.Enabled {
		return "http://" + s.Hostname + ":" + s.Port + path 
	}
	return ""
}

// TODO: This needs to be done...
// Used to determine if the dependent service is currently up.
// Ping should make multiple attempts to see if the external service is up before giving up and returning an error.
func (s *ViewHndlrService) Ping() (error) {
	// check if enabled
	// if so, then make a GET call to /ping of the URL()
	// if there is an error, then repeatedly call for X times with linear backoff until success
/* Keep and add to services...
        // ping loop
        var pingErr error
        for i := 0; i < 15; i++ {
                fmt.Printf("Pinging %s\n", DbHost)
                pingErr = server.DB.DB().Ping()
                if pingErr != nil {
                        fmt.Println(pingErr)
                } else {
                        fmt.Println("Ping replied.")
                        break;
                }
                time.Sleep(time.Duration(3) * time.Second)
        }
        if pingErr != nil {
                log.Fatal("This is the error:", err)
        }
*/

	return nil
}


func (s *ViewHndlrService) SendStatus(data Data) (error) {

	var err error
	if s.Enabled {
		//fmt.Printf("ViewHndlrService: Sending data to %s\n", s.URL("/data"))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(data)
		err = Post(s.URL("/data"), b)
	} else {
		fmt.Printf("ViewHndlrService is disabled. Unable to send status data.\n")
	}
	return err 

}

// DataRecord is a subset of data.Row
type DataRecord struct {
  Ts            int64   `json:"ts"`
  ValueA        int     `json:"valuea"`
  ValueB        int     `json:"valueb"`
}

type DataStateResp struct {
        TsStart uint64  `json:"start"`
        TsEnd   uint64  `json:"end"`
        Eui     string  `json:"eui"`
        Data    []DataRecord    `json:"data"`
}

// TODO: The following types should be extracted into a separate module...
type DataStateReq struct {
        TsStart uint64  `json:"start"`
        TsEnd   uint64  `json:"end"`
        Eui     string  `json:"eui"`
}

func (d *DataStateReq) Validate() error {
        if d.Eui == "" {
                return errors.New("Required Eui")
	}
	return nil
}

// TODO: Add error return value
// TODO: Validate Hostname and Port within reason
func (s *ViewHndlrService) Init(Hostname, Port string) {
	s.Hostname = Hostname
	s.Port = Port
	s.Enabled = true	
}

func (s *ViewHndlrService) GetData(req DataStateReq) (DataStateResp, error) {

	fmt.Printf("ViewHndlrService.GetData Eui: %s, start: %d, end: %d\n", req.Eui, req.TsStart, req.TsEnd)
	var resp DataStateResp
	if !s.Enabled {
		return resp, nil // This should be a custom error indicating the service was not initialized
	}
	reqBody, err := json.Marshal(req)
	if err != nil {
               	return resp, err 
	}
	url := s.URL("/state")
	client := &http.Client{}
	httpReq, err := http.NewRequest("GET", url, bytes.NewBuffer(reqBody))
	httpReq.Header.Set("Content-type","application/json")
	if err != nil {
               	return resp, err 
	}
        response, err := client.Do(httpReq)

	if err != nil {
               	return resp, err 
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
               		return resp, err 
		}
		err = json.Unmarshal(contents, &resp)
		if err != nil {
               		return resp, err 
		}
	}
	return resp, err
}

